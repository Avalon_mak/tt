package com.mak.dao;

import com.mak.data.dao.DAOResources;
import com.mak.data.dao.DepartmentDAO;
import com.mak.data.manager.DBManager;
import com.mak.data.model.Department;
import com.mak.exception.DBException;
import com.mak.manager.DBManagerTestImpl;
import org.apache.log4j.Logger;

import java.sql.*;

public class DepartmentDaoTestImpl extends DAOResources implements DepartmentDAO {
    private static final Logger LOG = Logger.getLogger(DepartmentDaoTestImpl.class);
    private DBManager manager = new DBManagerTestImpl();

    public DepartmentDaoTestImpl() throws DBException {
    }

    @Override
    public void create(Department department) {
        String query = "INSERT INTO DEPARTMENT (id, departmentName) VALUES (?, ?)";
        Connection connection = null;
        PreparedStatement pstmt = null;

        try {
            connection = this.manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setInt(1, department.getId());
                pstmt.setString(2, department.getName());
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            }
        } catch (DBException e) {
            LOG.error("Error while connect to DB: " + e.getMessage());
        }
    }

    public Department getDepartmentById(String id) {
        String query = "SELECT * FROM DEPARTMENT WHERE ID=(?)";
        Department result = new Department();
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            connection = this.manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, id);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    result.setId(rs.getInt(1));
                    result.setName(rs.getString(2));
                }
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            } finally {
                close(connection, pstmt, rs);
            }
        } catch (DBException e) {
            LOG.error("Error while connect to DB: " + e.getMessage());
        }
        return result;
    }

    public void createTablesForTest() {
        Statement st = null;
        Connection connection = null;
        try {
            connection = this.manager.getConnection();
            try {
                st = connection.createStatement();
                st.execute("CREATE TABLE DEPARTMENT (\n" +
                        "id INT AUTO_INCREMENT PRIMARY KEY,\n" +
                        "departmentName VARCHAR(255)\n" +
                        ");");
                st.execute("CREATE TABLE CO_WORKER (\n" +
                        "id INT AUTO_INCREMENT PRIMARY KEY,\n" +
                        "email VARCHAR(255),\n" +
                        "telephoneNumber INT,\n" +
                        "dateOfRegistration VARCHAR(255)\n" +
                        ");");
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            }
        } catch (DBException e) {
            LOG.error("Error while connect to DB: " + e.getMessage());
        }
    }

    public void deleteTables(){
        Statement st = null;
        Connection connection = null;

        try {
            connection = this.manager.getConnection();
            try {
                st = connection.createStatement();
                st.execute("DROP TABLE DEPARTMENT;");
                st.execute("DROP TABLE CO_WORKER;");
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            }
        } catch (DBException e) {
            LOG.error("Error while connect to DB: " + e.getMessage());
        }
    }


    @Override
    public void update(Department department) {
        String sql = "UPDATE DEPARTMENT SET departmentName=(?) WHERE id=(?)";
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(sql);
                pstmt.setString(1, department.getName());
                pstmt.setInt(2, department.getId());
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (DBException e) {
            LOG.error("Error while execute query: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }
    }

    @Override
    public void delete(String id) {
        String query = "DELETE FROM DEPARTMENT WHERE ID=(?)";
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, id);
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (DBException e) {
            LOG.error("Error while execute query: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }
    }
}
