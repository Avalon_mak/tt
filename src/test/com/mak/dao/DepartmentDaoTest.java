package com.mak.dao;

import com.mak.data.dao.DepartmentDAO;
import com.mak.data.dao.DepartmentDAOImpl;
import com.mak.data.model.Department;
import com.mak.exception.DBException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DepartmentDaoTest {

    private DepartmentDaoTestImpl dao = new DepartmentDaoTestImpl();

    public DepartmentDaoTest() throws DBException {
    }

    @Before
    public void setUp() {
        this.dao.createTablesForTest();
    }

    @After
    public void tearDown() {
        this.dao.deleteTables();
    }

    @Test
    public void saveDepartmentTest() {
        //WHEN
        Department expected = new Department();
        expected.setId(1);
        expected.setName("testDep");

        //THEN
        this.dao.create(expected);
        Department actual = this.dao.getDepartmentById(expected.getId().toString());

        //GIVEN
        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getName(), actual.getName());
    }

    @Test
    public void updateDepartmentTest() {
        //WHEN
        Department expected = new Department();
        expected.setId(1);
        expected.setName("testDep");
        this.dao.create(expected);

        //THEN
        expected.setName("testDepEdited");
        this.dao.update(expected);
        Department actual = this.dao.getDepartmentById(expected.getId().toString());

        //GIVEN
        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getName(), actual.getName());
    }

    @Test
    public void removeDepartmentTest() {
        //WHEN
        Department expected = new Department();
        expected.setId(1);
        expected.setName("testDep");
        this.dao.create(expected);

        //THEN
        this.dao.delete(expected.getId().toString());
        Department actual = this.dao.getDepartmentById(expected.getId().toString());

        //GIVEN
        Assert.assertEquals(null, actual.getId());
        Assert.assertEquals(null, actual.getName());
    }
}
