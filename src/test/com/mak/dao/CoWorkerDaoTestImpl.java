package com.mak.dao;

import com.mak.data.dao.CoWorkerDAO;
import com.mak.data.dao.DAOResources;
import com.mak.data.manager.DBManager;
import com.mak.data.model.CoWorker;
import com.mak.exception.DBException;
import com.mak.manager.DBManagerTestImpl;
import org.apache.log4j.Logger;

import java.sql.*;

public class CoWorkerDaoTestImpl extends DAOResources implements CoWorkerDAO {

    private static final Logger LOG = Logger.getLogger(CoWorkerDaoTestImpl.class);
    private DBManager manager = new DBManagerTestImpl();

    public CoWorkerDaoTestImpl() throws DBException {
    }

    @Override
    public void create(CoWorker coWorker) {
        Connection connection = null;
        String query = "INSERT INTO CO_WORKER (id, email, telephoneNumber, dateOfRegistration) VALUES (?, ?, ?, ?)";
        PreparedStatement pstmt = null;
        try {
            connection = this.manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setInt(1, coWorker.getId());
                pstmt.setString(2, coWorker.getEmail());
                pstmt.setInt(3, coWorker.getTelephoneNumber());
                pstmt.setString(4, coWorker.getDateOfRegistration());
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            }
        } catch (DBException e) {
            LOG.error("Error while connect to DB: " + e.getMessage());
        }

    }

    public CoWorker getCoWorkerById(String id) {
        String query = "SELECT * FROM CO_WORKER WHERE ID=(?)";
        CoWorker result = new CoWorker();
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            connection = this.manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, id);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    result.setId(rs.getInt(1));
                    result.setEmail(rs.getString(2));
                    result.setTelephoneNumber(rs.getInt(3));
                    result.setDateOfRegistration(rs.getString(4));
                }
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            } finally {
                close(connection, pstmt, rs);
            }
        } catch (DBException e) {
            LOG.error("Error while connect to DB: " + e.getMessage());
        }
        return result;
    }

    public void createTablesForTest() {
        Statement st = null;
        Connection connection = null;
        try {
            connection = this.manager.getConnection();
            try {
                st = connection.createStatement();
                st.execute("CREATE TABLE DEPARTMENT (\n" +
                        "id INT AUTO_INCREMENT PRIMARY KEY,\n" +
                        "departmentName VARCHAR(255)\n" +
                        ");");
                st.execute("CREATE TABLE CO_WORKER (\n" +
                        "id INT AUTO_INCREMENT PRIMARY KEY,\n" +
                        "email VARCHAR(255),\n" +
                        "telephoneNumber INT,\n" +
                        "dateOfRegistration VARCHAR(255)\n" +
                        ");");
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            }
        } catch (DBException e) {
            LOG.error("Error while connect to DB: " + e.getMessage());
        }
    }

    public void deleteTables(){
        Statement st = null;
        Connection connection = null;

        try {
            connection = this.manager.getConnection();
            try {
                st = connection.createStatement();
                st.execute("DROP TABLE DEPARTMENT;");
                st.execute("DROP TABLE CO_WORKER;");
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            }
        } catch (DBException e) {
            LOG.error("Error while connect to DB: " + e.getMessage());
        }
    }

    @Override
    public void update(CoWorker coWorker) {
        String sql = "UPDATE CO_WORKER SET email=(?), telephoneNumber=(?) WHERE id=(?)";
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(sql);
                pstmt.setString(1, coWorker.getEmail());
                pstmt.setInt(2, coWorker.getTelephoneNumber());
                pstmt.setInt(3, coWorker.getId());
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (DBException e) {
            LOG.error("Error while execute query: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }
    }

    @Override
    public void delete(String id) {
        String query = "DELETE FROM CO_WORKER WHERE ID=(?)";
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, id);
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (DBException e) {
            LOG.error("Error while execute query: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }
    }
}
