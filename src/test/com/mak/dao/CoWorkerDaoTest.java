package com.mak.dao;

import com.mak.data.model.CoWorker;
import com.mak.exception.DBException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CoWorkerDaoTest {

    private CoWorkerDaoTestImpl dao = new CoWorkerDaoTestImpl();

    public CoWorkerDaoTest() throws DBException {
    }

    @Before
    public void setUp() {
        this.dao.createTablesForTest();
    }

    @After
    public void tearDown() {
        this.dao.deleteTables();
    }

    @Test
    public void coWorkerSaveTest() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        //WHEN
        CoWorker expected = new CoWorker();
        expected.setId(1);
        expected.setEmail("test@mail.com");
        expected.setDepartmentId(1);
        expected.setTelephoneNumber(1111);
        expected.setDateOfRegistration("testDate");

        //THEN
        this.dao.create(expected);
        CoWorker actual = this.dao.getCoWorkerById(expected.getId().toString());

        //GIVEN
        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getEmail(), actual.getEmail());
        Assert.assertEquals(expected.getTelephoneNumber(), actual.getTelephoneNumber());
        Assert.assertEquals(expected.getDateOfRegistration(), actual.getDateOfRegistration());
    }

    @Test
    public void coWorkerUpdateTest() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        //WHEN
        CoWorker expected = new CoWorker();
        expected.setId(1);
        expected.setEmail("test@mail.com");
        expected.setDepartmentId(1);
        expected.setTelephoneNumber(1111);
        expected.setDateOfRegistration("testDate");
        this.dao.create(expected);

        //THEN
        expected.setEmail("testEdited@mail.com");
        this.dao.update(expected);
        CoWorker actual = this.dao.getCoWorkerById(expected.getId().toString());

        //GIVEN
        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getEmail(), actual.getEmail());
        Assert.assertEquals(expected.getTelephoneNumber(), actual.getTelephoneNumber());
        Assert.assertEquals(expected.getDateOfRegistration(), actual.getDateOfRegistration());
    }

    @Test
    public void coWorkerRemoveTest() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        //WHEN
        CoWorker expected = new CoWorker();
        expected.setId(1);
        expected.setEmail("test@mail.com");
        expected.setDepartmentId(1);
        expected.setTelephoneNumber(1111);
        expected.setDateOfRegistration("testDate");
        this.dao.create(expected);

        //THEN
        this.dao.delete(expected.getId().toString());
        CoWorker actual = this.dao.getCoWorkerById(expected.getId().toString());

        //GIVEN
        Assert.assertEquals(null, actual.getId());
        Assert.assertEquals(null, actual.getEmail());
        Assert.assertEquals(null, actual.getTelephoneNumber());
        Assert.assertEquals(null, actual.getDateOfRegistration());
    }
}
