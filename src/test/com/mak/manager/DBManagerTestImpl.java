package com.mak.manager;

import com.mak.data.manager.DBManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBManagerTestImpl implements DBManager {
    @Override
    public Connection getConnection() {
        Connection conn = null;

        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection("jdbc:h2:test",
                    "root", "root");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }


}
