package com.mak.service;

import com.mak.data.dto.CoWorkerDTO;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class CoWorkerValidationTest {

    @Test
    public void validationFailedTest() {
        //WHEN
        CoWorkerDTO coWorker = new CoWorkerDTO();
        coWorker.setEmail("test@mail");
        coWorker.setTelephoneNumber("");

        //THEN
        CoWorkerValidator validator = new CoWorkerValidator();
        Map<String, String> result = validator.validate(coWorker);

        //GIVEN
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void validationSuccessfulTest() {
        //WHEN
        CoWorkerDTO coWorker = new CoWorkerDTO();
        coWorker.setEmail("test@mail.com");
        coWorker.setTelephoneNumber("111");

        //THEN
        CoWorkerValidator validator = new CoWorkerValidator();
        Map<String, String> result = validator.validate(coWorker);

        //GIVEN
        Assert.assertEquals(0, result.size());
    }
}
