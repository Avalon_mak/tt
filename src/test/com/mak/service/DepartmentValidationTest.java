package com.mak.service;

import com.mak.data.dto.DepartmentDTO;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class DepartmentValidationTest {

    @Test
    public void validationFailedTest() {
        //WHEN
        DepartmentDTO departmentDTO = new DepartmentDTO();
        departmentDTO.setName("");

        //THEN
        DepartmentValidator validator = new DepartmentValidator();
        Map<String, String> result = validator.validate(departmentDTO);

        //GIVEN
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void validationSuccessfulTest() {
        //WHEN
        DepartmentDTO departmentDTO = new DepartmentDTO();
        departmentDTO.setName("testName");

        //THEN
        DepartmentValidator validator = new DepartmentValidator();
        Map<String, String> result = validator.validate(departmentDTO);

        //GIVEN
        Assert.assertEquals(0, result.size());
    }
}
