package com.mak.data.model;

import java.util.Map;

public class PersistResult {
    private Map<String, String> validationMessages;

    public Map<String, String> getValidationMessages() {
        return validationMessages;
    }

    public void setValidationMessages(Map<String, String> validationMessages) {
        this.validationMessages = validationMessages;
    }
}
