package com.mak.data.dao;

import com.mak.data.manager.DBManagerImpl;
import com.mak.data.model.CoWorker;
import com.mak.exception.DBException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CoWorkerDAOImpl extends DAOResources implements CoWorkerDAO {

    private DBManagerImpl manager = DBManagerImpl.getInstance();
    private static final Logger LOG = Logger.getLogger(CoWorkerDAOImpl.class);

    public List<CoWorker> getCoWorkersByDepId(String departmentId) {
        String query = "SELECT * FROM CO_WORKER WHERE departmentId=(?)";
        CoWorker cw = new CoWorker();
        List<CoWorker> result = new ArrayList<CoWorker>();
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, departmentId);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    cw.setId(rs.getInt(1));
                    cw.setEmail(rs.getString(2));
                    cw.setTelephoneNumber(rs.getInt(3));
                    cw.setDateOfRegistration(rs.getString(4));
                    cw.setDepartmentId(rs.getInt(5));
                    result.add(cw);
                    cw = new CoWorker();
                }
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());;
            }
        } catch (DBException e) {
            LOG.error("Error while connection to DB: " + e.getMessage());
        } finally {
            close(connection, pstmt, rs);
        }
        return result;
    }

    public Integer getCoWorkersNumberByDepId(String depId) {
        String query = "SELECT count(co_worker.id) FROM CO_WORKER WHERE co_worker.departmentId=(?);";
        Integer result = 0;
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet rs;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, depId);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    result = rs.getInt(1);
                }
                connection.commit();
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            }
        } catch (DBException e) {
            LOG.error("Error while connection to DB: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }

        return result;
    }

    public void create(CoWorker coWorker) {
        String query = "INSERT INTO CO_WORKER (email, telephoneNumber, dateOfRegistration, departmentId) VALUES (?, ?, ?, ?)";
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, coWorker.getEmail());
                pstmt.setInt(2, coWorker.getTelephoneNumber());
                pstmt.setString(3, coWorker.getDateOfRegistration());
                pstmt.setInt(4, coWorker.getDepartmentId());
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            }
        } catch (DBException e) {
            LOG.error("Error while connection to DB: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }
    }

    public CoWorker getCoWorkerById(String id) {
        String query = "SELECT * FROM CO_WORKER WHERE ID=(?)";
        CoWorker result = new CoWorker();
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, id);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    result.setId(rs.getInt(1));
                    result.setEmail(rs.getString(2));
                    result.setTelephoneNumber(rs.getInt(3));
                    result.setDateOfRegistration(rs.getString(4));
                    result.setDepartmentId(rs.getInt(5));
                }
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            }
        } catch (DBException e) {
            LOG.error("Error while connection to DB: " + e.getMessage());
        } finally {
            close(connection, pstmt, rs);
        }
        return result;
    }

    public void update(CoWorker coWorker) {
        String sql = "UPDATE CO_WORKER SET email=(?), telephoneNumber=(?) WHERE id=(?)";
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(sql);
                pstmt.setString(1, coWorker.getEmail());
                pstmt.setInt(2, coWorker.getTelephoneNumber());
                pstmt.setInt(3, coWorker.getId());
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (DBException e) {
            LOG.error("Error while execute query: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }
    }

    public void delete(String id) {
        String query = "DELETE FROM CO_WORKER WHERE ID=(?)";
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, id);
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (DBException e) {
            LOG.error("Error while execute query: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }
    }
}
