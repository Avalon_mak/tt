package com.mak.data.dao;

import org.apache.log4j.Logger;

import java.sql.*;

public abstract class DAOResources {
    private static final Logger LOG = Logger.getLogger(DAOResources.class);

    private void close(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                LOG.debug("Error while close resources:" + ex.getMessage());
            }
        }
    }

    private void close(PreparedStatement pstmt) {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                LOG.debug("Error while close resources:" + ex.getMessage());
            }
        }
    }
    public void close(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                LOG.debug("Error while close resources:" + ex.getMessage());
            }
        }
    }

    private void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                LOG.debug("Error while close resources:" + ex.getMessage());
            }
        }
    }

    public void close(Connection con, PreparedStatement pstmt, ResultSet rs) {
        LOG.debug("Close resources");
        close(rs);
        close(pstmt);
        close(con);
    }
}
