package com.mak.data.dao;

import com.mak.data.model.CoWorker;

public interface CoWorkerDAO {
    void create(CoWorker coWorker);
    void update(CoWorker coWorker);
    void delete(String id);
    CoWorker getCoWorkerById(String id);
}
