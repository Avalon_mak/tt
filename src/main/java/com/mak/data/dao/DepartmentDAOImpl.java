package com.mak.data.dao;

import com.mak.data.manager.DBManagerImpl;
import com.mak.data.model.Department;
import com.mak.exception.DBException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentDAOImpl extends DAOResources implements DepartmentDAO {
    private DBManagerImpl manager = DBManagerImpl.getInstance();
    private static final Logger LOG = Logger.getLogger(DepartmentDAOImpl.class);

    public List<Department> getDepartments(){
        String query = "SELECT * FROM DEPARTMENT";
        List<Department> result = new ArrayList<Department>();
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        Department department = new Department();
        try {
            connection = manager.getConnection();
            try {
                stmt = connection.createStatement();
                rs = stmt.executeQuery(query);
                while (rs.next()) {
                    Integer depId = rs.getInt(1);
                    department.setId(depId);
                    department.setName(rs.getString(2));
                    result.add(department);
                    department = new Department();
                }
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());;
            }
        } catch (DBException e) {
            LOG.error("Error while connection to DB: " + e.getMessage());
        } finally {
            close(stmt);
            close(connection, null, rs);
        }
        return result;
    }

    public void create(Department department){
        String query = "INSERT INTO DEPARTMENT (departmentName) VALUES (?)";
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, department.getName());
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            }
        } catch (DBException e) {
            LOG.error("Error while connection to DB: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }
    }

    public Department getDepartmentById(String id) {
        String query = "SELECT * FROM DEPARTMENT WHERE ID=(?)";
        Department result = new Department();
        Connection connection = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, id);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    result.setId(rs.getInt(1));
                    result.setName(rs.getString(2));
                }
            } catch (SQLException e) {
                LOG.error("Error while executing query: " + e.getMessage());
            }
        } catch (DBException e) {
            LOG.error("Error while connection to DB: " + e.getMessage());
        } finally {
            close(connection, pstmt, rs);
        }
        return result;
    }

    public void update(Department department) {
        String sql = "UPDATE DEPARTMENT SET departmentName=(?) WHERE id=(?)";
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(sql);
                pstmt.setString(1, department.getName());
                pstmt.setInt(2, department.getId());
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (DBException e) {
            LOG.error("Error while execute query: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }
    }

    public void delete(String id) {
        String query = "DELETE FROM DEPARTMENT WHERE ID=(?)";
        this.deleteDepartmentCoWorkers(id);
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, id);
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (DBException e) {
            LOG.error("Error while execute query: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }
    }

    private void deleteDepartmentCoWorkers(String departmentId){
        String query = "DELETE FROM CO_WORKER WHERE departmentId=(?)";
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = manager.getConnection();
            try {
                pstmt = connection.prepareStatement(query);
                pstmt.setString(1, departmentId);
                pstmt.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (DBException e) {
            LOG.error("Error while execute query: " + e.getMessage());
        } finally {
            close(connection, pstmt, null);
        }
    }
}
