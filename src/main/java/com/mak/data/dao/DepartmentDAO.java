package com.mak.data.dao;

import com.mak.data.model.Department;

public interface DepartmentDAO {
    void create(Department department);
    void update(Department department);
    void delete(String id);
    Department getDepartmentById(String id);
}
