package com.mak.data.dto;

import java.util.Map;

public class DepartmentDTO {
    private String id;
    private String name;
    private Integer coWorkersNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCoWorkersNumber() {
        return coWorkersNumber;
    }

    public void setCoWorkersNumber(Integer coWorkersNumber) {
        this.coWorkersNumber = coWorkersNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;    }
}
