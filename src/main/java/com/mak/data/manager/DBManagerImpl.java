package com.mak.data.manager;

import com.mak.exception.DBException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DBManagerImpl implements DBManager {
    private static DBManagerImpl instance;
    private DataSource ds;

    public static synchronized DBManagerImpl getInstance() {
        if (instance == null) {
            instance = new DBManagerImpl();
        }
        return instance;
    }

    public DBManagerImpl() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/TTDB");
        } catch (NamingException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public Connection getConnection() throws DBException {
        Connection con = null;
        try {
            con = ds.getConnection();
        } catch (SQLException ex) {
            throw new DBException("Cannot connect to DB cause " + ex.getMessage(), ex);
        }
        return con;
    }
}
