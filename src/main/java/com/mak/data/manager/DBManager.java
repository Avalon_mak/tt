package com.mak.data.manager;


import com.mak.exception.DBException;

import java.sql.Connection;

public interface DBManager {

    Connection getConnection()  throws DBException;
}
