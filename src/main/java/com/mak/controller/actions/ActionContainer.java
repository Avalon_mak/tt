package com.mak.controller.actions;

import java.util.Map;
import java.util.TreeMap;

public class ActionContainer {
    private static Map<String, Action> commands = new TreeMap<String, Action>();

    static {
        commands.put("/app/home", new HomeAction());
        commands.put("/app/error", new ErrorAction());
        commands.put("/app/create", new CreateDepartmentAction());
        commands.put("/app/editDepartment", new EditDepartmentAction());
        commands.put("/app/removeDepartment", new DeleteDepartmentAction());
        commands.put("/app/departmentCoWorkers", new CoWorkersListAction());
        commands.put("/app/create", new CreateCoWorkerAction());
        commands.put("/app/editCoWorker", new EditCoWorkerAction());
        commands.put("/app/removeCoWorker", new DeleteCoWorkerAction());
    }

    public static Action get(String commandName) {
        if (commandName == null || !commands.containsKey(commandName)) {
            return null;
        }

        return commands.get(commandName);
    }

}
