package com.mak.controller.actions;

import com.mak.controller.ControllerOperation;
import com.mak.controller.ControllerResult;
import com.mak.controller.HttpMethod;
import com.mak.data.dto.CoWorkerDTO;
import com.mak.service.CoWorkerService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteCoWorkerAction extends Action {

    private static final Logger LOG = Logger.getLogger(DeleteCoWorkerAction.class);
    private CoWorkerService service = new CoWorkerService();

    @Override
    public ControllerResult execute(HttpServletRequest request, HttpServletResponse response, HttpMethod method) {
        if (method == HttpMethod.GET) {
            return get(request, response);
        } else {
            return post(request, response);
        }
    }

    private ControllerResult get(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Co-Worker delete GET");
        return new ControllerResult(ControllerOperation.FORWARD, "/WEB-INF/jsp/home.jsp");
    }

    private ControllerResult post(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Co-Worker delete POST");

        String id = request.getParameter("id");
        CoWorkerDTO dto = this.service.deleteCoWorker(id);

        LOG.debug("Co-Worker delete successful");
        return new ControllerResult(ControllerOperation.REDIRECT, "/app/departmentCoWorkers?id=" + dto.getDepartmentId()
                + "&depName=" + dto.getDepartmentName());
    }
}
