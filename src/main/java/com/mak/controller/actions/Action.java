package com.mak.controller.actions;

import com.mak.controller.ControllerResult;
import com.mak.controller.HttpMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class Action {
    abstract public ControllerResult execute(HttpServletRequest request, HttpServletResponse response, HttpMethod method);
}
