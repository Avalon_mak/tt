package com.mak.controller.actions;

import com.mak.controller.ControllerOperation;
import com.mak.controller.ControllerResult;
import com.mak.controller.HttpMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ErrorAction extends Action {
    @Override
    public ControllerResult execute(HttpServletRequest request, HttpServletResponse response, HttpMethod method) {
        return new ControllerResult(ControllerOperation.FORWARD, "/WEB-INF/jsp/404Page.jsp");
    }
}
