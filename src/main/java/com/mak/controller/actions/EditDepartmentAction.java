package com.mak.controller.actions;

import com.mak.controller.ControllerOperation;
import com.mak.controller.ControllerResult;
import com.mak.controller.HttpMethod;
import com.mak.data.dto.DepartmentDTO;
import com.mak.data.model.Department;
import com.mak.data.model.PersistResult;
import com.mak.service.DepartmentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditDepartmentAction extends  Action {

    private static final Logger LOG = Logger.getLogger(EditDepartmentAction.class);
    private DepartmentService service = new DepartmentService();

    @Override
    public ControllerResult execute(HttpServletRequest request, HttpServletResponse response, HttpMethod method) {
        if (method == HttpMethod.GET) {
            return get(request, response);
        } else {
            return post(request, response);
        }
    }

    private ControllerResult get(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Department edit GET");

        String id = request.getParameter("id");

        Department department = service.getDepartment(id);

        request.setAttribute("pageLabel", "Edit Department");
        if (request.getAttribute("department") == null) {
            request.setAttribute("department", department);
        }
        request.setAttribute("actionType", "editDepartment");
        return new ControllerResult(ControllerOperation.FORWARD, "/WEB-INF/jsp/createEditDepartment.jsp");
    }

    private ControllerResult post(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Department edit POST");

        DepartmentDTO department = new DepartmentDTO();
        department.setId(request.getParameter("id"));
        department.setName(request.getParameter("departmentName"));

        PersistResult persistResult = this.service.updateDepartment(department);

        if (!persistResult.getValidationMessages().isEmpty()) {
            request.setAttribute("messages", persistResult.getValidationMessages());
            request.setAttribute("department", department);
            return get(request, response);
        }
        LOG.debug("Department edit successful");
        return new ControllerResult(ControllerOperation.REDIRECT, "/app/home");
    }
}
