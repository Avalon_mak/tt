package com.mak.controller.actions;

import com.mak.controller.ControllerOperation;
import com.mak.controller.ControllerResult;
import com.mak.controller.HttpMethod;
import com.mak.data.dto.DepartmentDTO;
import com.mak.data.model.PersistResult;
import com.mak.service.DepartmentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CreateDepartmentAction extends Action {

    private static final Logger LOG = Logger.getLogger(CreateDepartmentAction.class);
    private DepartmentService service = new DepartmentService();

    @Override
    public ControllerResult execute(HttpServletRequest request, HttpServletResponse response, HttpMethod method) {
        if (method == HttpMethod.GET) {
            return get(request, response);
        } else {
            return post(request, response);
        }
    }

    private ControllerResult get(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Create delete GET");

        request.setAttribute("pageLabel", "Create Department");
        request.setAttribute("actionType", "create");
        return new ControllerResult(ControllerOperation.FORWARD, "/WEB-INF/jsp/createEditDepartment.jsp");
    }

    private ControllerResult post(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Create delete POST");

        String departmentName = request.getParameter("departmentName");

        DepartmentDTO dto = new DepartmentDTO();
        dto.setName(departmentName);

        PersistResult persistResult = this.service.createDepartment(dto);

        if (!persistResult.getValidationMessages().isEmpty()) {
            request.setAttribute("messages", persistResult.getValidationMessages());
            request.setAttribute("department", dto);
            return get(request, response);
        }
        LOG.debug("Department create successful");
        return new ControllerResult(ControllerOperation.REDIRECT, "/app/home");
    }
}
