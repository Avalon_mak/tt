package com.mak.controller.actions;

import com.mak.controller.ControllerOperation;
import com.mak.controller.ControllerResult;
import com.mak.controller.HttpMethod;
import com.mak.data.dto.CoWorkerDTO;
import com.mak.data.model.PersistResult;
import com.mak.service.CoWorkerService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditCoWorkerAction extends Action {

    private static final Logger LOG = Logger.getLogger(EditCoWorkerAction.class);
    private CoWorkerService service = new CoWorkerService();

    @Override
    public ControllerResult execute(HttpServletRequest request, HttpServletResponse response, HttpMethod method) {
        if (method == HttpMethod.GET) {
            return get(request, response);
        } else {
            return post(request, response);
        }
    }

    private ControllerResult get(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Co-worker edit GET");

        String id = request.getParameter("id");

        CoWorkerDTO dto = service.getCoWorker(id);

        request.setAttribute("pageLabel", "Edit Co-Worker");
        request.setAttribute("departmentData", dto);
        if (request.getAttribute("coWorker") == null) {
            request.setAttribute("coWorker", dto);
        }
        request.setAttribute("actionType", "editCoWorker");

        return new ControllerResult(ControllerOperation.FORWARD, "/WEB-INF/jsp/createEditCoWorker.jsp");
    }

    private ControllerResult post(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Co-worker edit POST");

        CoWorkerDTO coWorker = new CoWorkerDTO();
        coWorker.setId(request.getParameter("id"));
        coWorker.setEmail(request.getParameter("coWorkerEmail"));
        coWorker.setTelephoneNumber(request.getParameter("coWorkerNumber"));
        coWorker.setDateOfRegistration(request.getParameter("coWorkerDate"));
        coWorker.setDepartmentId(request.getParameter("departmentId"));

        PersistResult persistResult = this.service.updateCoWorker(coWorker);

        if (!persistResult.getValidationMessages().isEmpty()) {
            request.setAttribute("messages", persistResult.getValidationMessages());
            request.setAttribute("coWorker", coWorker);
            return get(request, response);
        }
        LOG.debug("Co-worker edit successful");
        return new ControllerResult(ControllerOperation.REDIRECT, "/app/departmentCoWorkers?id=" + request.getParameter("departmentId"));
    }
}
