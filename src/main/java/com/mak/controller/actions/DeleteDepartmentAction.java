package com.mak.controller.actions;

import com.mak.controller.ControllerOperation;
import com.mak.controller.ControllerResult;
import com.mak.controller.HttpMethod;
import com.mak.service.DepartmentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteDepartmentAction extends Action {

    private static final Logger LOG = Logger.getLogger(DeleteDepartmentAction.class);
    private DepartmentService service = new DepartmentService();

    @Override
    public ControllerResult execute(HttpServletRequest request, HttpServletResponse response, HttpMethod method) {
        if (method == HttpMethod.GET) {
            return get(request, response);
        } else {
            return post(request, response);
        }
    }

    private ControllerResult get(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Department delete GET");
        return new ControllerResult(ControllerOperation.FORWARD, "/WEB-INF/jsp/home.jsp");
    }

    private ControllerResult post(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Department delete POST");
        String id = request.getParameter("id");
        this.service.deleteDepartment(id);

        LOG.debug("Department delete successful");
        return new ControllerResult(ControllerOperation.REDIRECT, "/app/home");
    }
}
