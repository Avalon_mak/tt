package com.mak.controller.actions;

import com.mak.controller.ControllerOperation;
import com.mak.controller.ControllerResult;
import com.mak.controller.HttpMethod;
import com.mak.data.dto.CoWorkerDTO;
import com.mak.data.model.PersistResult;
import com.mak.service.CoWorkerService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class CreateCoWorkerAction extends Action {

    private static final Logger LOG = Logger.getLogger(CreateCoWorkerAction.class);
    private CoWorkerService service = new CoWorkerService();

    @Override
    public ControllerResult execute(HttpServletRequest request, HttpServletResponse response, HttpMethod method) {
        if (method == HttpMethod.GET) {
            return get(request, response);
        } else {
            return post(request, response);
        }
    }

    private ControllerResult get(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Create Co-Worker GET");

        CoWorkerDTO dto = service.departmentDetails(request.getParameter("departmentId"));

        request.setAttribute("pageLabel", "Create Co-Worker");
        request.setAttribute("actionType", "create");
        request.setAttribute("departmentData", dto);
        return new ControllerResult(ControllerOperation.FORWARD, "/WEB-INF/jsp/createEditCoWorker.jsp");
    }

    private ControllerResult post(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Create Co-Worker POST");

        CoWorkerDTO coWorker = new CoWorkerDTO();
        coWorker.setEmail(request.getParameter("coWorkerEmail"));
        coWorker.setTelephoneNumber(request.getParameter("coWorkerNumber"));
        coWorker.setDepartmentId(request.getParameter("departmentId"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        coWorker.setDateOfRegistration(dateFormat.format(date));

        PersistResult persistResult = this.service.createCoWorker(coWorker);
        if (!persistResult.getValidationMessages().isEmpty()) {
            request.setAttribute("messages", persistResult.getValidationMessages());
            request.setAttribute("coWorker", coWorker);
            return get(request, response);
        }

        return new ControllerResult(ControllerOperation.REDIRECT, "/app/departmentCoWorkers?id=" + request.getParameter("departmentId"));
    }
}
