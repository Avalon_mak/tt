package com.mak.controller.actions;

import com.mak.controller.ControllerOperation;
import com.mak.controller.ControllerResult;
import com.mak.controller.HttpMethod;
import com.mak.data.dto.CoWorkerDTO;
import com.mak.data.model.CoWorker;
import com.mak.service.CoWorkerService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class CoWorkersListAction extends Action {

    private CoWorkerService service = new CoWorkerService();
    private static final Logger LOG = Logger.getLogger(CoWorkersListAction.class);

    @Override
    public ControllerResult execute(HttpServletRequest request, HttpServletResponse response, HttpMethod method) {
        if (method == HttpMethod.GET) {
            return get(request, response);
        } else {
            return post(request, response);
        }
    }

    private ControllerResult get(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Co-Workers List GET");

        String departmentId = request.getParameter("id");


        List<CoWorker> coWorkers = this.service.coWorkersList(departmentId);

        CoWorkerDTO dto = this.service.departmentDetails(departmentId);

        request.setAttribute("pageLabel", dto.getDepartmentName() + "Co-Workers");
        request.setAttribute("departmentName", dto.getDepartmentName());
        request.setAttribute("coWorkersList", coWorkers);
        request.setAttribute("pageLabel", dto.getDepartmentName() + " Co-Workers List");
        request.setAttribute("departmentId", departmentId);
        return new ControllerResult(ControllerOperation.FORWARD, "/WEB-INF/jsp/coWorkersList.jsp");
    }

    private ControllerResult post(HttpServletRequest request, HttpServletResponse response) {
        return new ControllerResult(ControllerOperation.FORWARD, "/WEB-INF/jsp/wrongPage.jsp");
    }
}
