package com.mak.controller.actions;

import com.mak.controller.ControllerOperation;
import com.mak.controller.ControllerResult;
import com.mak.controller.HttpMethod;
import com.mak.data.dto.DepartmentDTO;
import com.mak.data.model.Department;
import com.mak.service.DepartmentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class HomeAction extends Action {

    private static final Logger LOG = Logger.getLogger(HomeAction.class);
    private DepartmentService service = new DepartmentService();

    @Override
    public ControllerResult execute(HttpServletRequest request, HttpServletResponse response, HttpMethod method) {
        if (method == HttpMethod.GET) {
            return get(request, response);
        } else {
            return post(request, response);
        }
    }

    private ControllerResult get(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Home (Department List) GET");
        List<DepartmentDTO> departments = new ArrayList<DepartmentDTO>();
        departments = this.service.departmentsList();
        request.setAttribute("departmentsList", departments);
        request.setAttribute("pageLabel", "Departments List");
        return new ControllerResult(ControllerOperation.FORWARD, "/WEB-INF/jsp/home.jsp");
    }

    private ControllerResult post(HttpServletRequest request, HttpServletResponse response) {
        return new ControllerResult(ControllerOperation.FORWARD, "/WEB-INF/jsp/wrongPage.jsp");
    }
}
