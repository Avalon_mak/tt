package com.mak.controller;

public class ControllerResult {

    private final String actionPath;
    private final ControllerOperation operation;

    public ControllerResult(ControllerOperation operation, String actionPath) {
        this.actionPath = actionPath;
        this.operation = operation;
    }

    public String getActionPath() {
        return actionPath;
    }

    public ControllerOperation getOperation() {
        return operation;
    }
    
}
