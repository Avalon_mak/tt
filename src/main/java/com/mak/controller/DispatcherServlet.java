package com.mak.controller;

import com.mak.controller.actions.Action;
import com.mak.controller.actions.ActionContainer;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Main servlet-class. Accepting all requests from browser and call controllers by uri.
 *
 * @author Ihor Makivskyi
 */
public class DispatcherServlet extends HttpServlet {
    private static final long serialVersionUID = 4512145721675262470L;
    private static final Logger LOG = Logger.getLogger(DispatcherServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp, HttpMethod.GET);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp, HttpMethod.POST);
    }

    private void process(HttpServletRequest request, HttpServletResponse response, HttpMethod method) {

        String uri = request.getRequestURI();
        String actionPath = uri.substring(request.getContextPath().length());

        Action a = ActionContainer.get(actionPath);
        if (a != null) {
            ControllerResult result = a.execute(request, response, method);
            actionPath = result.getActionPath();
            try {
                if (result.getOperation() == ControllerOperation.FORWARD) {
                    request.getRequestDispatcher(actionPath).forward(request, response);
                } else {
                    response.sendRedirect(request.getContextPath() + result.getActionPath());
                }
            } catch (ServletException e) {
                LOG.error("Controller: ServletException: " + e.getMessage());
            } catch (IOException e) {
                LOG.error("Controller: IOException: " + e.getMessage());
            }
        } else {
            try {
                response.sendRedirect(request.getContextPath() + "/app/error");
            } catch (IOException e) {
                LOG.error("Controller: IOException" + e.getMessage());
            }
        }
    }
}
