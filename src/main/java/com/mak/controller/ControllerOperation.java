package com.mak.controller;

public enum ControllerOperation {

    FORWARD,
    REDIRECT;

}
