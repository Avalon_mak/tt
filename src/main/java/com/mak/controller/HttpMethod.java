package com.mak.controller;
/**
 * HTTP methods for controllers.
 *
 * @author Ihor Makivskyi
 */
public enum HttpMethod {

    GET,
    POST;
    
}
