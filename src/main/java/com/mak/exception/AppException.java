package com.mak.exception;

public class AppException extends Exception {
    /**
     * Exception class parent.
     *
     * @author Ihor Makivskyi
     */
    private static final long serialVersionUID = 8288779062647218916L;

    public AppException() {
        super();
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppException(String message) {
        super(message);
    }
}
