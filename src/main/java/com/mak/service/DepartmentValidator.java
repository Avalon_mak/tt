package com.mak.service;

import com.mak.data.dto.DepartmentDTO;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class DepartmentValidator {

    private static final Logger LOG = Logger.getLogger(DepartmentValidator.class);

    public Map<String, String> validate(DepartmentDTO department) {
        Map<String, String> result = new HashMap<String, String>();
        LOG.debug("Department name validation start.");
        if (department.getName().length() == 0) {
            result.put("departmentName", " Field cannot be empty!");
        }

        return result;
    }
}
