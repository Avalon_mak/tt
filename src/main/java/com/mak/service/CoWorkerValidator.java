package com.mak.service;

import com.mak.data.dto.CoWorkerDTO;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class CoWorkerValidator{
    private static final Logger LOG = Logger.getLogger(CoWorkerValidator.class);

    public Map<String, String> validate(CoWorkerDTO coWorker) {
        Map<String, String> result = new HashMap<String, String>();

        LOG.debug("Email validation start.");
        String [] email = coWorker.getEmail().split("@");
        if (email.length > 1) {
            if (!email[1].contains(".")) {
                result.put("coWorkerEmail", " Wrong data!");
            }
        } else {
            result.put("coWorkerEmail", " Wrong data!");
        }
        LOG.debug("Telephone Number validation start.");
        if (coWorker.getTelephoneNumber().length() == 0) {
            result.put("coWorkerNumber", " Field cannot be empty!");
        } else {
            if (!isParseble(coWorker.getTelephoneNumber())) {
                result.put("coWorkerNumber", " Wrong data!");
            }
        }

        return  result;
    }

    private boolean isParseble(String stringNumber) {
        try {
            Integer.parseInt(stringNumber);
        } catch (NumberFormatException ex) {
            LOG.debug("Validator(Parse Check): wrong data in number filed: " + stringNumber);
            return false;
        }

        return true;
    }
}
