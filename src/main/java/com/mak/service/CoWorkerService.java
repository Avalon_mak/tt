package com.mak.service;

import com.mak.data.dao.CoWorkerDAOImpl;
import com.mak.data.dao.DepartmentDAOImpl;
import com.mak.data.dto.CoWorkerDTO;
import com.mak.data.model.CoWorker;
import com.mak.data.model.Department;
import com.mak.data.model.PersistResult;
import org.apache.log4j.Logger;

import java.util.List;

public class CoWorkerService {

    private static final Logger LOG = Logger.getLogger(CoWorkerService.class);
    private CoWorkerDAOImpl dao = new CoWorkerDAOImpl();
    private DepartmentDAOImpl departmentDao = new DepartmentDAOImpl();

    public List<CoWorker> coWorkersList(String departmentId) {
        return this.dao.getCoWorkersByDepId(departmentId);
    }

    public CoWorkerDTO departmentDetails(String departmentId) {
        CoWorkerDTO dto = new CoWorkerDTO();
        Department department = departmentDao.getDepartmentById(departmentId);

        dto.setDepartmentId(departmentId);
        dto.setDepartmentName(department.getName());

        return dto;
    }

    public CoWorkerDTO deleteCoWorker(String id) {
        CoWorker coWorker = this.dao.getCoWorkerById(id);
        CoWorkerDTO dto = departmentDetails(coWorker.getDepartmentId().toString());
        this.dao.delete(id);

        return dto;
    }
    public PersistResult createCoWorker(CoWorkerDTO coWorkerDTO) {
        PersistResult persistResult = new PersistResult();
        CoWorkerValidator validator = new CoWorkerValidator();
        persistResult.setValidationMessages(validator.validate(coWorkerDTO));
        if (persistResult.getValidationMessages().isEmpty()) {
            CoWorker coWorker = new CoWorker();
            coWorker.setEmail(coWorkerDTO.getEmail());
            coWorker.setDepartmentId(Integer.parseInt(coWorkerDTO.getDepartmentId()));
            coWorker.setTelephoneNumber(parseNumber(coWorkerDTO.getTelephoneNumber()));
            coWorker.setDateOfRegistration(coWorkerDTO.getDateOfRegistration());
            this.dao.create(coWorker);
        }

        return persistResult;
    }

    public CoWorkerDTO getCoWorker(String coWorkerId) {
        CoWorker coWorker = this.dao.getCoWorkerById(coWorkerId);
        CoWorkerDTO dto = new CoWorkerDTO();
        Department department = this.departmentDao.getDepartmentById(coWorker.getDepartmentId().toString());
        dto.setEmail(coWorker.getEmail());
        dto.setDateOfRegistration(coWorker.getDateOfRegistration());
        dto.setId(coWorker.getId().toString());
        dto.setTelephoneNumber(coWorker.getTelephoneNumber().toString());
        dto.setDepartmentId(department.getId().toString());
        dto.setDepartmentName(department.getName());

        return dto;
    }

    public PersistResult updateCoWorker(CoWorkerDTO coWorkerDto) {
        PersistResult persistResult = new PersistResult();

        CoWorkerValidator validator = new CoWorkerValidator();
        persistResult.setValidationMessages(validator.validate(coWorkerDto));

        if (persistResult.getValidationMessages().isEmpty()) {
            CoWorker coWorker = new CoWorker();
            coWorker.setId(Integer.parseInt(coWorkerDto.getId()));
            coWorker.setEmail(coWorkerDto.getEmail());
            coWorker.setDepartmentId(Integer.parseInt(coWorkerDto.getDepartmentId()));
            coWorker.setTelephoneNumber(parseNumber(coWorkerDto.getTelephoneNumber()));
            coWorker.setDateOfRegistration(coWorkerDto.getDateOfRegistration());
            this.dao.update(coWorker);
        }
        return persistResult;
    }

    private Integer parseNumber(String number) {
        Integer result = null;
        try {
            result = Integer.parseInt(number);
        } catch (NumberFormatException ex) {
            LOG.debug("Parse: wrong data in number filed: " + number);
        }

        return result;
    }
}
