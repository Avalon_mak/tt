package com.mak.service;

import com.mak.data.dao.CoWorkerDAOImpl;
import com.mak.data.dao.DepartmentDAOImpl;
import com.mak.data.dto.DepartmentDTO;
import com.mak.data.model.Department;
import com.mak.data.model.PersistResult;

import java.util.ArrayList;
import java.util.List;

public class DepartmentService {
    private DepartmentDAOImpl dao = new DepartmentDAOImpl();
    private CoWorkerDAOImpl coWorkerDAO = new CoWorkerDAOImpl();

    public List<DepartmentDTO> departmentsList() {
        DepartmentDTO dto = new DepartmentDTO();
        List<Department> departments = this.dao.getDepartments();
        List<DepartmentDTO> result = new ArrayList<DepartmentDTO>();
        for (Department department : departments) {
            dto.setName(department.getName());
            dto.setId(department.getId().toString());
            dto.setCoWorkersNumber(this.coWorkerDAO.getCoWorkersNumberByDepId(department.getId().toString()));
            result.add(dto);
            dto = new DepartmentDTO();
        }
        return result;
    }

    public void deleteDepartment(String id) {
        this.dao.delete(id);
    }

    public PersistResult createDepartment(DepartmentDTO dto) {
        PersistResult result = new PersistResult();
        DepartmentValidator validator = new DepartmentValidator();
        result.setValidationMessages(validator.validate(dto));
        dto.setName(dto.getName());

        if (result.getValidationMessages().isEmpty()) {
            Department department = new Department();
            department.setName(dto.getName());
            dao.create(department);
        }
        return result;
    }

    public Department getDepartment(String id) {
        return this.dao.getDepartmentById(id);
    }

    public PersistResult updateDepartment(DepartmentDTO dto) {
        PersistResult persistResult = new PersistResult();

        DepartmentValidator validator = new DepartmentValidator();
        persistResult.setValidationMessages(validator.validate(dto));

        if (persistResult.getValidationMessages().isEmpty()) {
            Department department = new Department();
            department.setId(Integer.parseInt(dto.getId()));
            department.setName(dto.getName());
            this.dao.update(department);
        }
        return persistResult;
    }
}
