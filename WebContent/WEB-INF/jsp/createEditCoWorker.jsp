<html>
<%@ include file='/WEB-INF/jsp/jspf/head.jspf'%>
<body>
  <div class='row'>
    <h4>${pageLabel}</h4>
    <div class='col-xs-4'></div>
    <form action='${actionType}' method='POST'>
      <div class='row'>
        <table class='col-xs-4' id='editPage'>
          <tr>
            <input type='hidden' name='id' value='${coWorker.id}' />
            <input type='hidden' name='departmentId' value='${departmentData.getDepartmentId()}' />
            <input type='hidden' name='coWorkerDate' value='${coWorker.dateOfRegistration}' />
            <td>
              <label>Email:</label>
            </td>
            <td id='controls'>
              <input name='coWorkerEmail' value='${coWorker.email}' />
              <div id='validator'>
                <m:error field='coWorkerEmail' messages='${messages}' />
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <label>Telephone Number:</label>
            </td>
            <td id='controls'>
              <input name='coWorkerNumber' value='${coWorker.telephoneNumber}' />
              <div id='validator'>
                <m:error field='coWorkerNumber' messages='${messages}' />
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class='doneButton row'>
        <input type='submit' class='btn button btn-default' value='Done' />
      </div>
    </form>
  </div>
  <div class='row'>
    <form action='departmentCoWorkers?id=${departmentData.getDepartmentId()}&depName=${departmentData.getDepartmentName()}' action='GET'>
      <input name='id' type='hidden' value='${departmentData.getDepartmentId()}' />
      <input name='departmentName' type='hidden' value='${departmentData.getDepartmentName()}' />
      <input type='submit' class='button btn btn-default' value='Back' />
    </form>
  </div>
</body>
</html>