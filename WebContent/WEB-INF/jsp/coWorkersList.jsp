<html>
<%@ include file='/WEB-INF/jsp/jspf/head.jspf'%>
<body>
  <div class='row'>
    <h4>${pageLabel}</h4>
    <div class='col-xs-4'></div>
    <table class='col-xs-4' id='listTable'>
      <tr id='headers'>
        <th>Email</th>
        <th>Telephone Number</th>
        <th>Date of Registration</th>
        <th>Controls</th>
      </tr>
      <c:if test='${coWorkersList.size() == 0}'>
        <td>There is nothing to show</td>
      </c:if>
      <c:if test='${coWorkersList.size() > 0}'>
        <c:forEach var='coWorker' items='${coWorkersList}'>
          <tr>
            <input type='hidden' name='id' value='${coWorker.id}'>
            <td>${coWorker.email}</td>
            <td>${coWorker.telephoneNumber}</td>
            <td>${coWorker.dateOfRegistration}</td>
            <td id='controls'>
              <a href='editCoWorker?id=${coWorker.id}' class='btn editButton btn-default'>Edit</a>
              <form action='removeCoWorker' method='POST'>
                <input type='hidden' name="id" value='${coWorker.id}' />
                <input type='submit' class='btn btn-default' value='Remove' />
              </form>
            </td>
          </tr>
        </c:forEach>
      </c:if>
    </table>
  </div>
  <div class='row' id='content'>
    <form action='createCoWorker' method='GET'>
      <input type='hidden' name="departmentId" value='${departmentId}' />
      <input type='submit' class='button btn btn-default' value='Create Co-Worker' />
    </form>
    <a href='home' class='button btn btn-default'>Back</a>
  </div>
</body>
</html>