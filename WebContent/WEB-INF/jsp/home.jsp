<html>
<%@ include file='/WEB-INF/jsp/jspf/head.jspf'%>
<body>
  <div class='row'>
    <h4>Departments</h4>
    <div class='col-xs-4'></div>
    <table class='col-xs-4' id='listTable'>
        <tr id='headers'>
          <th>Department name</th>
          <th>Number of co-workers</th>
          <th>Controls</th>
        </tr>
        <c:if test='${departmentsList.size() == 0}'>
          <tr>There is nothing to show</tr>
        </c:if>
        <c:if test='${departmentsList.size() > 0}'>
          <c:forEach var='department' items='${departmentsList}'>
            <tr>
              <input type='hidden' name='id' value='${department.id}'>
              <td>${department.name}</td>
              <td>${department.coWorkersNumber}</td>
              <td id='controls'>
                <form action='editDepartment' method='GET'>
                  <input type='hidden' name="id" value='${department.id}' />
                  <input type='submit' class='btn btn-default' value='Edit' />
                </form>
                <form action='removeDepartment' method='POST'>
                  <input type='hidden' name="id" value='${department.id}' />
                  <input type='submit' class='btn btn-default' value='Remove' />
                </form>
                <form action='departmentCoWorkers' method='GET'>
                  <input type='hidden' name="id" value='${department.id}' />
                  <input type='submit' class='btn btn-default' value='Co-Workers List' />
                </form>
              </td>
            </tr>
          </c:forEach>
        </c:if>
    </table>
  </div>
  <div class='row' id='content'>
    <a href='createDepartment' class='button btn btn-default'>Create Department</a>
  </div>
</body>
</html>