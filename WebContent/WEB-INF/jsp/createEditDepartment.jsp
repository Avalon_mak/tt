<html>
<%@ include file='/WEB-INF/jsp/jspf/head.jspf'%>
<body>
  <div class='row'>
    <h4>${pageLabel}</h4>
    <div class='col-xs-4'></div>
    <div class='col-xs-4' id='editPageTable'>
      <form action='${actionType}' method='POST'>
        <input type='hidden' name="id" value='${department.id}' />
        <label>Department name:</label>
        <input name='departmentName' value='${department.name}' />
        <div id='validator'>
          <m:error field="departmentName" messages="${messages}" />
        </div>
          <input type='submit' class='btn button btn-default' value='Done' />
      </form>
      <a href='home' class='button btn btn-default'>Back</a>
    </div>
  </div>
</body>
</html>