# Configuring and launch project

For build project you can use Gradle.  
If you use gradle - redefine Tomcat location for build target on **gradle.properties** file in project root folder, and use "gradlew clean sync".  

Also, you can build project manually. For that yo need:  
 - Go to **%your_tomcat_directory%/webapps** and create folder for project  
 - Go to **%project_directory%/WebContent** and copy **WEB-INF, META-INF and css** folders into folder in folder, what you create in Tomcat webapps.  
 - Compile sources and copy binaries into **%your_tomcat_directory%/webapps/%project_folder%/WEB-INF/classes**  
 - In **%your_tomcat_directory%/webapps/%project_folder%/META-INF** folder in file **context.xml** defined setting *(login, password and url)* for database connection.  

In **root folder of project** you can find **sql** folder with database scheme file.