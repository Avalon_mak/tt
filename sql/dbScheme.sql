CREATE DATABASE departments_administrate
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;
  
USE departments_administrate;
  
CREATE TABLE DEPARTMENT (
id INT AUTO_INCREMENT PRIMARY KEY,
departmentName VARCHAR(255)
);


CREATE TABLE CO_WORKER (
id INT AUTO_INCREMENT PRIMARY KEY,
email VARCHAR(255),
telephoneNumber INT,
dateOfRegistration VARCHAR(255),
departmentId INT REFERENCES DEPARTMENTS
);
